package com.dimingo.red_detector;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener {

    private static final String TAG = "OCVSample :: Activity";
    int i = 0;
    private CameraBridgeViewBase mOpenCvCameraView;
    private TextView textView;
    private TextView muteStatusTextView;
    private MediaPlayer mPlayer;
    private boolean mIsJavaCamera = true;
    private boolean isMute = false;
    private MenuItem mItemSwitchCamera = null;
    Mat mResult;
    // These variables are used (at the moment) to fix camera orientation from 270degree to 0degree
    Mat mRgba;
    Mat mRgbaF;
    Mat mRgbaT;


    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                // Start camera preview when loading is successful
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("App", "onCreate");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //ask for authorisation
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 50);
        }
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.camera_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        FloatingActionButton fabMute = findViewById(R.id.fab_mute);
        fabMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Mute the alarm sound", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                isMute = true;
                setMuteStatusText(muteStatusTextView, "Sound OFF");
            }
        });

        FloatingActionButton fabUnMute = findViewById(R.id.fab_unmute);
        fabUnMute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Enable the alarm sound", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                isMute = false;
                setMuteStatusText(muteStatusTextView, "Sound ON");
            }
        });

        textView = findViewById(R.id.text_view);
        muteStatusTextView = findViewById(R.id.mute_status_text_view);
        setMuteStatusText(muteStatusTextView, "Sound ON");

        mPlayer = MediaPlayer.create(this, R.raw.sound_1);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("App", "onPause");
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("App", "onResume");
        if (!OpenCVLoader.initDebug())
            Log.e("onResume:OpenCv", "Unable to load OpenCV");
        else
            Log.d("onResume:OpenCv", "OpenCV loaded");
        mOpenCvCameraView.enableView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("App", "onCameraViewStarted");
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        Log.d("App", "onCameraViewStarted");
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mRgbaF = new Mat(height, width, CvType.CV_8UC4);
        mRgbaT = new Mat(width, width, CvType.CV_8UC4);
        mResult = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        Log.d("App", "onCameraViewStopped");
        mRgba.release();
    }

    @Override
    public Mat onCameraFrame(Mat inputFrame) {
        Log.d("App", "onCameraFrame");
        mRgba = inputFrame;
        // Rotate mRgba 90 degrees
        Core.transpose(mRgba, mRgbaT);
        Imgproc.resize(mRgbaT, mRgbaF, mRgbaF.size(), 0,0, 0);
        Core.flip(mRgbaF, mRgba, 1);
        Mat src = mRgba;
        src.copyTo(mResult);
        Imgproc.cvtColor(src, src, Imgproc.COLOR_RGB2HSV); // convert to HSV
        Imgproc.medianBlur(src, src, 5);
        Core.inRange(src, new Scalar(150, 100, 100), new Scalar(180, 255, 255), src); // red color
        Mat hierarchy = Mat.zeros(new Size(5,5), CvType.CV_8UC1);
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(src, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_TC89_L1);
        Scalar color = new Scalar(200, 20, 100);
        // Imgproc.drawContours (mResult, contours, -1, color, 10);
        int i = 0;
        int index = -1;
        double area = 0;
        for (i = 0; i < contours.size(); i++) {
            double tmp = Imgproc.contourArea(contours.get(i));
            if (area < tmp) {
                area = tmp;
                index = i;
            }
        }
        if (index != -1) { // found red color
            MatOfPoint ptmat = contours.get(index);
            color = new Scalar(0, 200, 0);
            MatOfPoint2f ptmat2 = new MatOfPoint2f(ptmat.toArray());
            RotatedRect bbox = Imgproc.minAreaRect(ptmat2);
//            Imgproc.circle(mResult, bbox.center, 10, color, -1); // original
            Imgproc.circle(mResult, bbox.center, 10, color, -1);
            setText(textView, "RED Alarm!!!");
            setVisibility(textView, View.VISIBLE);
            if (isMute) {
                setMuteStatusText(muteStatusTextView, "Sound OFF");
            } else {
                mPlayer.start();
                setMuteStatusText(muteStatusTextView, "Sound ON");
            }
        } else {
            setVisibility(textView, View.INVISIBLE);
        }
        return mResult;
    }

    Mat greenDetect (Mat mat) {
        Log.d("App", "greenDetect");
        Mat mat1 = new Mat();
        Core.inRange(mat, new Scalar(30,70,70), new Scalar(70,250,250), mat1);
        return mat1;
    }

    private void setText(final TextView text, final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
                text.setTextColor(Color.RED);
                text.setTextSize(40);
                text.setTypeface(null, Typeface.BOLD);
            }
        });
    }

    private void setMuteStatusText(final TextView text, final String value){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
                text.setTextColor(Color.BLACK);
                text.setTextSize(20);
                text.setTypeface(null, Typeface.BOLD);
            }
        });
    }

    private void setVisibility(final TextView text, final int visibility){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setVisibility(visibility);
            }
        });
    }

}
